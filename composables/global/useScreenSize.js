import { ref, onMounted, onUnmounted } from 'vue';

export function useScreenSize() {
    const isSmallScreen = ref(window.innerWidth < 1024); // Adjusted for tablets and smaller screens

    const updateScreenSize = () => {
        isSmallScreen.value = window.innerWidth < 1024;
    };

    onMounted(() => {
        window.addEventListener('resize', updateScreenSize);
        updateScreenSize(); // Initialize on mount
    });

    onUnmounted(() => {
        window.removeEventListener('resize', updateScreenSize);
    });

    return { isSmallScreen };
}